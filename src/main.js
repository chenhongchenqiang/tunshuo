// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import ElementUI from 'element-ui'
import router from './router'
import store from './store'
// import ajax from '../src/script/request/api.js';
import 'element-ui/lib/theme-chalk/index.css'
import VueCookies from 'vue-cookies'
Vue.use(VueCookies)
Vue.use(ElementUI)
// Vue.prototype.$ajax = ajax;//通过Vue.prototype设置全局变量
Vue.config.productionTip = false
/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
})
