import Vue from 'vue'
import Router from 'vue-router'
import Index from '@/script/views/index'
import Login from '@/script/views/login'
import Main from '@/script/components/Main'

Vue.use(Router)
let routes = [{
  path: '/',
  name: '投诉',
  component: Main,
  iconCls: 'fa fa-address-card',
  children: [{
    path: '/',
    component: Index,
    name: '动态'
  }, {
    path: '/2',
    component: Index,
    name: '评论'
  }, {
    path: '/1',
    component: Index,
    name: '组队'
  }]
}, {
  path: '/login',
  name: 'Login',
  component: Login
}]

import {
  SystemRouter
} from './system'

for (let i in SystemRouter) {
  routes.push(SystemRouter[i])
}

const router = new Router({
  routes: routes
})

export default router;
// export default new Router({
//   routes: [
//     {
//       path: '/',
//       name: 'Index',
//       component: Index
//     },
//     {
//       path: '/helloWorld',
//       name: 'HelloWorld',
//       component: HelloWorld
//     },
//     {
//       path: '/login',
//       name: 'Login',
//       component: Login
//     }
//   ]
// })
