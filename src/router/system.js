import Main from '@/script/components/Main'
import Contract from '@/script/views/active'
import IndFor from '@/script/views/indFor'
import BackEit from '@/script/views/backEit'
import BackAdd from '@/script/views/backAdd'
import BackList from '@/script/views/backList'
import ForbidList from '@/script/views/forbidList'
const SystemRouter = [{
  path: '/system',
  name: '审核',
  component: Main,
  iconCls: 'fa fa-address-card',
  children: [{
    path: '/system/contract',
    component: Contract,
    name: '音频审核'
  },{
    path: '/system/indFor',
    component: IndFor,
    name: '认证审核'
  }]
},{
  path: '/system',
  name: '封号',
  component: Main,
  iconCls: 'fa fa-address-card',
  children: [{
    path: '/system/forbidList',
    component: ForbidList,
    name: '封号列表'
  }]
},{
  path: '/back',
  name: '后街',
  component: Main,
  iconCls: 'fa fa-address-card',
  children: [{
    path: '/back/list',
    component: BackList,
    name: '列表'
  },{
    path: '/back/add',
    component: BackAdd,
    name: '新增/编辑'
  }]
}]

export {
  SystemRouter
}
